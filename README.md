<html><head><title>CPU-Z HTML report file</title></head><body bgcolor=FFFFFF>
<font face="Verdana" color="#000000">
<table border=0 width="100%" cellspacing="0" cellpadding="5">
<tr align="left">
<td>
<p><a href="http://www.cpuid.com" target=blank><img src="http://www.cpuid.com/medias/images/en/header-logo.jpg" border="0" width="180" height="66"></a></p>
</td>
</tr>
<tr><td><table border="0" width="100%" cellspacing="2" cellpadding="2" bgcolor="#FFFFFF">
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>CPU-Z</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Binaries</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>CPU-Z version</small></td><td valign="center"><small><font color="#0000A0">1.84.0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Processors</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Number of sockets</small></td><td valign="center"><small><font color="#0000A0">1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Number of threads</small></td><td valign="center"><small><font color="#0000A0">4</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>APICs</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Socket 0</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	-- Core 0</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		-- Thread 0</small></td><td valign="center"><small><font color="#0000A0">0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	-- Core 1</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		-- Thread 1</small></td><td valign="center"><small><font color="#0000A0">2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	-- Core 2</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		-- Thread 2</small></td><td valign="center"><small><font color="#0000A0">4</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	-- Core 3</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		-- Thread 3</small></td><td valign="center"><small><font color="#0000A0">6</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Timers</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	ACPI timer</small></td><td valign="center"><small><font color="#0000A0">3.580 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Perf timer</small></td><td valign="center"><small><font color="#0000A0">3.516 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Sys timer</small></td><td valign="center"><small><font color="#0000A0">1.000 KHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Processors Information</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Socket 1</small></td><td valign="center"><small><font color="#0000A0">ID = 0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Number of cores</small></td><td valign="center"><small><font color="#0000A0">4 (max 4)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Number of threads</small></td><td valign="center"><small><font color="#0000A0">4 (max 4)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Name</small></td><td valign="center"><small><font color="#0000A0">Intel Core i3 8100</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Codename</small></td><td valign="center"><small><font color="#0000A0">Coffee Lake</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Specification</small></td><td valign="center"><small><font color="#0000A0">Intel(R) Core(TM) i3-8100 CPU @ 3.60GHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Package (platform ID)</small></td><td valign="center"><small><font color="#0000A0">Socket 1151 LGA (0x1)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	CPUID</small></td><td valign="center"><small><font color="#0000A0">6.E.B</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Extended CPUID</small></td><td valign="center"><small><font color="#0000A0">6.9E</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Core Stepping</small></td><td valign="center"><small><font color="#0000A0">B0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Technology</small></td><td valign="center"><small><font color="#0000A0">14 nm</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	TDP Limit</small></td><td valign="center"><small><font color="#0000A0">65.0 Watts</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Tjmax</small></td><td valign="center"><small><font color="#0000A0">100.0 °C</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Core Speed</small></td><td valign="center"><small><font color="#0000A0">3589.5 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Multiplier x Bus Speed</small></td><td valign="center"><small><font color="#0000A0">36.0 x 99.7 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Base frequency (cores)</small></td><td valign="center"><small><font color="#0000A0">99.7 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Base frequency (ext.)</small></td><td valign="center"><small><font color="#0000A0">99.7 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Stock frequency</small></td><td valign="center"><small><font color="#0000A0">3600 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max frequency</small></td><td valign="center"><small><font color="#0000A0">3600 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Instructions sets</small></td><td valign="center"><small><font color="#0000A0">MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, EM64T, VT-x, AES, AVX, AVX2, FMA3</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Microcode Revision</small></td><td valign="center"><small><font color="#0000A0">0x5E</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	L1 Data cache</small></td><td valign="center"><small><font color="#0000A0">4 x 32 KBytes, 8-way set associative, 64-byte line size</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	L1 Instruction cache</small></td><td valign="center"><small><font color="#0000A0">4 x 32 KBytes, 8-way set associative, 64-byte line size</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	L2 cache</small></td><td valign="center"><small><font color="#0000A0">4 x 256 KBytes, 4-way set associative, 64-byte line size</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	L3 cache</small></td><td valign="center"><small><font color="#0000A0">6 MBytes, 12-way set associative, 64-byte line size</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max CPUID level</small></td><td valign="center"><small><font color="#0000A0">00000016h</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max CPUID ext. level</small></td><td valign="center"><small><font color="#0000A0">80000008h</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Cache descriptor</small></td><td valign="center"><small><font color="#0000A0">Level 1, D, 32 KB, 2 thread(s)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Cache descriptor</small></td><td valign="center"><small><font color="#0000A0">Level 1, I, 32 KB, 2 thread(s)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Cache descriptor</small></td><td valign="center"><small><font color="#0000A0">Level 2, U, 256 KB, 2 thread(s)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Cache descriptor</small></td><td valign="center"><small><font color="#0000A0">Level 3, U, 6 MB, 16 thread(s)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	FID/VID Control</small></td><td valign="center"><small><font color="#0000A0">yes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	IBRS</small></td><td valign="center"><small><font color="#0000A0">not supported</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	IBPB</small></td><td valign="center"><small><font color="#0000A0">not supported</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	STIBP</small></td><td valign="center"><small><font color="#0000A0">not supported</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	RDCL_NO</small></td><td valign="center"><small><font color="#0000A0">no</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	IBRS_ALL</small></td><td valign="center"><small><font color="#0000A0">not supported</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Turbo Mode</small></td><td valign="center"><small><font color="#0000A0">not supported</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max non-turbo ratio</small></td><td valign="center"><small><font color="#0000A0">36x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max turbo ratio</small></td><td valign="center"><small><font color="#0000A0">36x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max efficiency ratio</small></td><td valign="center"><small><font color="#0000A0">8x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	O/C bins</small></td><td valign="center"><small><font color="#0000A0">none</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Ratio 1 core</small></td><td valign="center"><small><font color="#0000A0">36x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Ratio 2 cores</small></td><td valign="center"><small><font color="#0000A0">36x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Ratio 3 cores</small></td><td valign="center"><small><font color="#0000A0">36x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Ratio 4 cores</small></td><td valign="center"><small><font color="#0000A0">36x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	IA Voltage Mode</small></td><td valign="center"><small><font color="#0000A0">PCU adaptive</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	IA Voltage Offset</small></td><td valign="center"><small><font color="#0000A0">0 mV</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	GT Voltage Mode</small></td><td valign="center"><small><font color="#0000A0">PCU adaptive</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	GT Voltage Offset</small></td><td valign="center"><small><font color="#0000A0">0 mV</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	LLC/Ring Voltage Mode</small></td><td valign="center"><small><font color="#0000A0">PCU adaptive</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	LLC/Ring Voltage Offset</small></td><td valign="center"><small><font color="#0000A0">0 mV</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Agent Voltage Mode</small></td><td valign="center"><small><font color="#0000A0">PCU adaptive</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Agent Voltage Offset</small></td><td valign="center"><small><font color="#0000A0">0 mV</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	TDP Level</small></td><td valign="center"><small><font color="#0000A0">65.0 W @ 36x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 0</small></td><td valign="center"><small><font color="#0000A0">35 degC (95 degF) (Package)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 1</small></td><td valign="center"><small><font color="#0000A0">35 degC (95 degF) (Core #0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 2</small></td><td valign="center"><small><font color="#0000A0">35 degC (95 degF) (Core #1)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 3</small></td><td valign="center"><small><font color="#0000A0">35 degC (95 degF) (Core #2)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 4</small></td><td valign="center"><small><font color="#0000A0">35 degC (95 degF) (Core #3)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 0</small></td><td valign="center"><small><font color="#0000A0">1.09 Volts (VID)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 1</small></td><td valign="center"><small><font color="#0000A0">+0.00 Volts (IA Offset)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 2</small></td><td valign="center"><small><font color="#0000A0">+0.00 Volts (GT Offset)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 3</small></td><td valign="center"><small><font color="#0000A0">+0.00 Volts (LLC/Ring Offset)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 4</small></td><td valign="center"><small><font color="#0000A0">+0.00 Volts (System Agent Offset)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Power 00</small></td><td valign="center"><small><font color="#0000A0">9.99 W (Package)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Power 01</small></td><td valign="center"><small><font color="#0000A0">6.62 W (IA Cores)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Power 02</small></td><td valign="center"><small><font color="#0000A0">n.a. (GT)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Power 03</small></td><td valign="center"><small><font color="#0000A0">3.37 W (Uncore)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Power 04</small></td><td valign="center"><small><font color="#0000A0">n.a. (DRAM)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Clock Speed 0</small></td><td valign="center"><small><font color="#0000A0">3589.45 MHz (Core #0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Clock Speed 1</small></td><td valign="center"><small><font color="#0000A0">3589.45 MHz (Core #1)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Clock Speed 2</small></td><td valign="center"><small><font color="#0000A0">3589.45 MHz (Core #2)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Clock Speed 3</small></td><td valign="center"><small><font color="#0000A0">3589.45 MHz (Core #3)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Core 0 max ratio</small></td><td valign="center"><small><font color="#0000A0">36.0 (effective 36.0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Core 1 max ratio</small></td><td valign="center"><small><font color="#0000A0">36.0 (effective 36.0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Core 2 max ratio</small></td><td valign="center"><small><font color="#0000A0">36.0 (effective 36.0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Core 3 max ratio</small></td><td valign="center"><small><font color="#0000A0">36.0 (effective 36.0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Chipset</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Northbridge</small></td><td valign="center"><small><font color="#0000A0">Intel Coffee Lake rev. 08</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Southbridge</small></td><td valign="center"><small><font color="#0000A0">Intel Z370 rev. 00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Graphic Interface</small></td><td valign="center"><small><font color="#0000A0">PCI-Express</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>PCI-E Link Width</small></td><td valign="center"><small><font color="#0000A0">x16</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>PCI-E Max Link Width</small></td><td valign="center"><small><font color="#0000A0">x16</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Memory Type</small></td><td valign="center"><small><font color="#0000A0">DDR4</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Memory Size</small></td><td valign="center"><small><font color="#0000A0">8 GBytes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Channels</small></td><td valign="center"><small><font color="#0000A0">Single</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Memory Frequency</small></td><td valign="center"><small><font color="#0000A0">1063.6 MHz (1:16)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>CAS# latency (CL)</small></td><td valign="center"><small><font color="#0000A0">15.0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>RAS# to CAS# delay (tRCD)</small></td><td valign="center"><small><font color="#0000A0">15</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>RAS# Precharge (tRP)</small></td><td valign="center"><small><font color="#0000A0">15</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Cycle Time (tRAS)</small></td><td valign="center"><small><font color="#0000A0">36</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Row Refresh Cycle Time (tRFC)</small></td><td valign="center"><small><font color="#0000A0">278</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Command Rate (CR)</small></td><td valign="center"><small><font color="#0000A0">2T</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Uncore Frequency</small></td><td valign="center"><small><font color="#0000A0">3290.3 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Host Bridge</small></td><td valign="center"><small><font color="#0000A0">0x3E1F</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Memory SPD</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DIMM #</small></td><td valign="center"><small><font color="#0000A0">1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	SMBus address</small></td><td valign="center"><small><font color="#0000A0">0x53</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Memory type</small></td><td valign="center"><small><font color="#0000A0">DDR4</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Module format</small></td><td valign="center"><small><font color="#0000A0">UDIMM</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Manufacturer (ID)</small></td><td valign="center"><small><font color="#0000A0"> (7F7F7F7F7F7F7F5D000000)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Size</small></td><td valign="center"><small><font color="#0000A0">8192 MBytes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max bandwidth</small></td><td valign="center"><small><font color="#0000A0">DDR4-2132 (1066 MHz)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Part number</small></td><td valign="center"><small><font color="#0000A0">GR2133D464L15/8G  </font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Manufacturing date</small></td><td valign="center"><small><font color="#0000A0">Week 36/Year 17</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Nominal Voltage</small></td><td valign="center"><small><font color="#0000A0">1.20 Volts</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	EPP</small></td><td valign="center"><small><font color="#0000A0">no</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	XMP</small></td><td valign="center"><small><font color="#0000A0">no</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	AMP</small></td><td valign="center"><small><font color="#0000A0">no</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>JEDEC timings table</small></td><td valign="center"><small><font color="#0000A0">CL-tRCD-tRP-tRAS-tRC @ frequency</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #1</small></td><td valign="center"><small><font color="#0000A0">9.0-9-9-22-31 @ 666 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #2</small></td><td valign="center"><small><font color="#0000A0">11.0-11-11-27-38 @ 814 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #3</small></td><td valign="center"><small><font color="#0000A0">12.0-12-12-30-42 @ 888 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #4</small></td><td valign="center"><small><font color="#0000A0">13.0-13-13-32-45 @ 962 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #5</small></td><td valign="center"><small><font color="#0000A0">14.0-14-14-35-49 @ 1037 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #6</small></td><td valign="center"><small><font color="#0000A0">15.0-15-15-36-50 @ 1066 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #7</small></td><td valign="center"><small><font color="#0000A0">16.0-15-15-36-50 @ 1066 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #8</small></td><td valign="center"><small><font color="#0000A0">18.0-15-15-36-50 @ 1066 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	JEDEC #9</small></td><td valign="center"><small><font color="#0000A0">19.0-15-15-36-50 @ 1066 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Monitoring</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Mainboard Model</small></td><td valign="center"><small><font color="#0000A0">Z370 HD3-CF (0x00000289 - 0x5E265BE0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>LPCIO</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>LPCIO Vendor</small></td><td valign="center"><small><font color="#0000A0">ITE</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>LPCIO Model</small></td><td valign="center"><small><font color="#0000A0">IT8686</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>LPCIO Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x90</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>LPCIO Chip ID</small></td><td valign="center"><small><font color="#0000A0">0x8686</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>LPCIO Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Hardware Monitors</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Hardware monitor</small></td><td valign="center"><small><font color="#0000A0">ITE IT8686</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 0</small></td><td valign="center"><small><font color="#0000A0">1.08 Volts [0x5A] (CPU VCORE)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 1</small></td><td valign="center"><small><font color="#0000A0">2.04 Volts [0xAA] (VIN1)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 2</small></td><td valign="center"><small><font color="#0000A0">1.99 Volts [0xA6] (VIN2)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 3</small></td><td valign="center"><small><font color="#0000A0">1.99 Volts [0xA6] (VIN3)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 5</small></td><td valign="center"><small><font color="#0000A0">1.06 Volts [0x58] (VIN5)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 6</small></td><td valign="center"><small><font color="#0000A0">1.21 Volts [0x65] (VIN6)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 7</small></td><td valign="center"><small><font color="#0000A0">1.68 Volts [0x8C] (VIN7)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 0</small></td><td valign="center"><small><font color="#0000A0">26 degC (78 degF) [0x1A] (TMPIN0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 1</small></td><td valign="center"><small><font color="#0000A0">29 degC (84 degF) [0x1D] (TMPIN1)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 2</small></td><td valign="center"><small><font color="#0000A0">25 degC (77 degF) [0x19] (TMPIN2)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 3</small></td><td valign="center"><small><font color="#0000A0">25 degC (77 degF) [0x19] (TMPIN3)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 4</small></td><td valign="center"><small><font color="#0000A0">32 degC (89 degF) [0x20] (TMPIN4)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 5</small></td><td valign="center"><small><font color="#0000A0">26 degC (78 degF) [0x1A] (TMPIN5)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Fan 0</small></td><td valign="center"><small><font color="#0000A0">964 RPM [0x2BC] (CPU)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Fan PWM 0</small></td><td valign="center"><small><font color="#0000A0">0 pc [0x0] (FANPWM0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Fan PWM 1</small></td><td valign="center"><small><font color="#0000A0">0 pc [0x0] (FANPWM1)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Fan PWM 2</small></td><td valign="center"><small><font color="#0000A0">0 pc [0x0] (FANPWM2)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Hardware monitor</small></td><td valign="center"><small><font color="#0000A0">NVIDIA NVAPI</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Voltage 0</small></td><td valign="center"><small><font color="#0000A0">0.94 Volts [0x3AF] (GPU)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Power 0</small></td><td valign="center"><small><font color="#0000A0">17.17 pc (GPU)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Temperature 0</small></td><td valign="center"><small><font color="#0000A0">29 degC (84 degF) [0x1D] (GPU)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Fan PWM 0</small></td><td valign="center"><small><font color="#0000A0">30 pc [0x1E] (FANPWMIN0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Clock Speed 0</small></td><td valign="center"><small><font color="#0000A0">966.62 MHz [0x3C6] (Graphics)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Clock Speed 1</small></td><td valign="center"><small><font color="#0000A0">3004.32 MHz [0x3C6] (Memory)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>PCI Devices</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">Host Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 0 (0x00), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0x3E1F</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x08</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 1 (0x01), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0x1901</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x08</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">System Device</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 8 (0x08), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0x1911</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">USB Controller</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 20 (0x14), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2AF</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">Communication Device</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 22 (0x16), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2BA</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">Serial ATA Controller</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 23 (0x17), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA282</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 27 (0x1B), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2E7</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 27 (0x1B), function 2 (0x02)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2E9</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 27 (0x1B), function 3 (0x03)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2EA</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 27 (0x1B), function 4 (0x04)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2EB</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 28 (0x1C), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA290</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 28 (0x1C), function 2 (0x02)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA292</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 28 (0x1C), function 4 (0x04)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA294</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to PCI Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 29 (0x1D), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA298</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xF0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">PCI to ISA Bridge</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 31 (0x1F), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2C9</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">Memory Controller</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 31 (0x1F), function 2 (0x02)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2A1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">Multimedia device</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 31 (0x1F), function 3 (0x03)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2F0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">SMBus Controller</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 31 (0x1F), function 4 (0x04)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0xA2A3</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">Ethernet Controller</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 0 (0x00), device 31 (0x1F), function 6 (0x06)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x8086</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0x15B8</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0x00</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">VGA Controller</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 1 (0x01), device 0 (0x00), function 0 (0x00)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x10DE</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0x1380</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xA2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Description</small></td><td valign="center"><small><font color="#0000A0">Multimedia device</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Location</small></td><td valign="center"><small><font color="#0000A0">bus 1 (0x01), device 0 (0x00), function 1 (0x01)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Common header</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x10DE</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0x0FBC</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xA1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>DMI</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI BIOS</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	vendor</small></td><td valign="center"><small><font color="#0000A0">American Megatrends Inc.</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	version</small></td><td valign="center"><small><font color="#0000A0">F3</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	date</small></td><td valign="center"><small><font color="#0000A0">09/06/2017</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	ROM size</small></td><td valign="center"><small><font color="#0000A0">16384 KB</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI System Information</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	manufacturer</small></td><td valign="center"><small><font color="#0000A0">Gigabyte Technology Co. Ltd.</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	product</small></td><td valign="center"><small><font color="#0000A0">Z370 HD3</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	version</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	serial</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	UUID</small></td><td valign="center"><small><font color="#0000A0">{03D502E0-045E-050B-D806-5D0700080009}</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	SKU</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	family</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Baseboard</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	vendor</small></td><td valign="center"><small><font color="#0000A0">Gigabyte Technology Co. Ltd.</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	model</small></td><td valign="center"><small><font color="#0000A0">Z370 HD3-CF</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	revision</small></td><td valign="center"><small><font color="#0000A0">x.x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	serial</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI System Enclosure</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	manufacturer</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	chassis type</small></td><td valign="center"><small><font color="#0000A0">Desktop</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	chassis serial</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J1A1 (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">PS2Mouse (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	port type</small></td><td valign="center"><small><font color="#0000A0">Mouse Port</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">PS/2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J1A1 (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">Keyboard (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	port type</small></td><td valign="center"><small><font color="#0000A0">Keyboard Port</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">PS/2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J2A1 (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">TV Out (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">Mini Centronics Type-14</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J2A2A (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">COM A (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	port type</small></td><td valign="center"><small><font color="#0000A0">Serial Port 16550A</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">DB-9 male</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J2A2B (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">Video (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	port type</small></td><td valign="center"><small><font color="#0000A0">Video Port</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">DB-15 female</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J3A1 (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">USB1 (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	port type</small></td><td valign="center"><small><font color="#0000A0">USB</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">Access Bus (USB)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J3A1 (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">USB2 (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	port type</small></td><td valign="center"><small><font color="#0000A0">USB</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">Access Bus (USB)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J3A1 (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">USB3 (external)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	port type</small></td><td valign="center"><small><font color="#0000A0">USB</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	connector</small></td><td valign="center"><small><font color="#0000A0">Access Bus (USB)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J9A1 - TPM HDR (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J9C1 - PCIE DOCKING CONN (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J2B3 - CPU FAN (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J6C2 - EXT HDMI (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J3C1 - GMCH FAN (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J1D1 - ITP (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J9E2 - MDC INTPSR (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J9E4 - MDC INTPSR (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J9E3 - LPC HOT DOCKING (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J9E1 - SCAN MATRIX (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J9G1 - LPC SIDE BAND (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J8F1 - UNIFIED (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J6F1 - LVDS (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J2F1 - LAI FAN (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J2G1 - GFX VID (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Port Connector</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J1G6 - AC JACK (internal)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Extension Slot</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J6B2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">A5</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	populated</small></td><td valign="center"><small><font color="#0000A0">yes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Extension Slot</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J6B1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">A5</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	populated</small></td><td valign="center"><small><font color="#0000A0">yes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Extension Slot</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J6D1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">A5</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	populated</small></td><td valign="center"><small><font color="#0000A0">yes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Extension Slot</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J7B1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">A5</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	populated</small></td><td valign="center"><small><font color="#0000A0">yes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Extension Slot</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">J8B4</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">A5</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	populated</small></td><td valign="center"><small><font color="#0000A0">yes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI OEM Strings</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	string[0]</small></td><td valign="center"><small><font color="#0000A0">Default string</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Physical Memory Array</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	location</small></td><td valign="center"><small><font color="#0000A0">Motherboard</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	usage</small></td><td valign="center"><small><font color="#0000A0">System Memory</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	correction</small></td><td valign="center"><small><font color="#0000A0">None</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	max capacity</small></td><td valign="center"><small><font color="#0000A0">65536 MBytes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	max# of devices</small></td><td valign="center"><small><font color="#0000A0">4</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Memory Device</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">ChannelA-DIMM0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	format</small></td><td valign="center"><small><font color="#0000A0">unknown</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">unknown</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Memory Device</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">ChannelA-DIMM1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	format</small></td><td valign="center"><small><font color="#0000A0">unknown</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">unknown</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Memory Device</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">ChannelB-DIMM0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	format</small></td><td valign="center"><small><font color="#0000A0">unknown</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">unknown</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Memory Device</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	designation</small></td><td valign="center"><small><font color="#0000A0">ChannelB-DIMM1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	format</small></td><td valign="center"><small><font color="#0000A0">DIMM</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	type</small></td><td valign="center"><small><font color="#0000A0">unknown</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	total width</small></td><td valign="center"><small><font color="#0000A0">64 bits</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	data width</small></td><td valign="center"><small><font color="#0000A0">64 bits</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	size</small></td><td valign="center"><small><font color="#0000A0">8192 MBytes</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DMI Processor</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	manufacturer</small></td><td valign="center"><small><font color="#0000A0">Intel(R) Corporation</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	model</small></td><td valign="center"><small><font color="#0000A0">Intel(R) Core(TM) i3-8100 CPU @ 3.60GHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	clock speed</small></td><td valign="center"><small><font color="#0000A0">3600.0 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	FSB speed</small></td><td valign="center"><small><font color="#0000A0">100.0 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	multiplier</small></td><td valign="center"><small><font color="#0000A0">36.0x</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Graphics</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Number of adapters</small></td><td valign="center"><small><font color="#0000A0">1</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Graphic APIs</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>API</small></td><td valign="center"><small><font color="#0000A0">NVIDIA I/O</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>API</small></td><td valign="center"><small><font color="#0000A0">NVIDIA NVAPI</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Display Adapters</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Display adapter 0</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	ID</small></td><td valign="center"><small><font color="#0000A0">0x1070107</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Name</small></td><td valign="center"><small><font color="#0000A0">NVIDIA GeForce GTX 750 Ti</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Board Manufacturer</small></td><td valign="center"><small><font color="#0000A0">NVIDIA Corporation</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision</small></td><td valign="center"><small><font color="#0000A0">A2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Codename</small></td><td valign="center"><small><font color="#0000A0">GM107</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Technology</small></td><td valign="center"><small><font color="#0000A0">28 nm</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Memory size</small></td><td valign="center"><small><font color="#0000A0">2 GB</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Memory type</small></td><td valign="center"><small><font color="#0000A0">GDDR5</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	PCI device</small></td><td valign="center"><small><font color="#0000A0">bus 1 (0x1), device 0 (0x0), function 0 (0x0)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vendor ID</small></td><td valign="center"><small><font color="#0000A0">0x10DE (0x10DE)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model ID</small></td><td valign="center"><small><font color="#0000A0">0x1380 (0x1380)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Revision ID</small></td><td valign="center"><small><font color="#0000A0">0xA2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Performance Level</small></td><td valign="center"><small><font color="#0000A0">0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		Core clock</small></td><td valign="center"><small><font color="#0000A0">966.6 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		Memory clock</small></td><td valign="center"><small><font color="#0000A0">3004.3 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Win32_VideoController</small></td><td valign="center"><small><font color="#0000A0">AdapterRAM = 0x80000000 (2147483648)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Win32_VideoController</small></td><td valign="center"><small><font color="#0000A0">DriverVersion = 23.21.13.9135</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Win32_VideoController</small></td><td valign="center"><small><font color="#0000A0">DriverDate = 03/23/2018</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Monitor 0</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model</small></td><td valign="center"><small><font color="#0000A0">SyncMaster (Samsung)</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	ID</small></td><td valign="center"><small><font color="#0000A0">SAM01B7</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Serial</small></td><td valign="center"><small><font color="#0000A0">HMELA19435</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Manufacturing Date</small></td><td valign="center"><small><font color="#0000A0">Week 40, Year 2006</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Size</small></td><td valign="center"><small><font color="#0000A0">17.1 inches</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max Resolution</small></td><td valign="center"><small><font color="#0000A0">1280 x 1024 @ 60 Hz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Horizontal Freq. Range</small></td><td valign="center"><small><font color="#0000A0">30-81 kHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vertical Freq. Range</small></td><td valign="center"><small><font color="#0000A0">56-75 Hz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max Pixel Clock</small></td><td valign="center"><small><font color="#0000A0">140 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Gamma Factor</small></td><td valign="center"><small><font color="#0000A0">2.2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Monitor 1</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Model</small></td><td valign="center"><small><font color="#0000A0">MP59G (LG Electronics (GoldStar))</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	ID</small></td><td valign="center"><small><font color="#0000A0">GSM5B34</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Serial</small></td><td valign="center"><small><font color="#0000A0"></font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Manufacturing Date</small></td><td valign="center"><small><font color="#0000A0">Week 1, Year 2014</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Size</small></td><td valign="center"><small><font color="#0000A0">21.7 inches</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max Resolution</small></td><td valign="center"><small><font color="#0000A0">1920 x 1080 @ 74 Hz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Horizontal Freq. Range</small></td><td valign="center"><small><font color="#0000A0">30-85 kHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Vertical Freq. Range</small></td><td valign="center"><small><font color="#0000A0">40-75 Hz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Max Pixel Clock</small></td><td valign="center"><small><font color="#0000A0">180 MHz</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;&nbsp;&nbsp;&nbsp;	Gamma Factor</small></td><td valign="center"><small><font color="#0000A0">2.2</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
<tr valign="top" bgcolor="#E0E0FF"><td width="300"><small><b>Software</b></small></td><td valign="center"><small><small><font color="#0000A0">&nbsp;</font></small></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>Windows Version</small></td><td valign="center"><small><font color="#0000A0">Microsoft Windows 10 (10.0) Professional 64-bit   (Build 16299) </font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>DirectX Version</small></td><td valign="center"><small><font color="#0000A0">12.0</font></small></td></tr>
<tr valign="top" bgcolor="#FFFFFF"><td width="300"><small>&nbsp;</small></td><td valign="center"><small><font color="#0000A0">&nbsp;</font></small></td></tr>
</table></td></tr>
</font></body></html>
